﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sample1
{
    [TestClass]
    public class UnitTest1
    {
        public class AddToIncrementOrDecrementTransform : ExpressionVisitor
        {
            protected override Expression VisitBinary(BinaryExpression node)
            {
                ParameterExpression param = null;
                ConstantExpression constant = null;

                if (node.NodeType == ExpressionType.Add)
                {

                    if (node.Left.NodeType == ExpressionType.Parameter)
                    {
                        param = (ParameterExpression)node.Left;
                    }
                    else if (node.Left.NodeType == ExpressionType.Constant)
                    {
                        constant = (ConstantExpression)node.Left;
                    }

                    if (node.Right.NodeType == ExpressionType.Parameter)
                    {
                        param = (ParameterExpression)node.Right;
                    }
                    else if (node.Right.NodeType == ExpressionType.Constant)
                    {
                        constant = (ConstantExpression)node.Right;
                    }

                    if (param != null && constant != null && constant.Type == typeof(int) && (int)constant.Value == 1)
                    {
                        return Expression.Increment(param);
                    }
                }

                if (node.NodeType == ExpressionType.Subtract)
                {
                    if (node.Left.NodeType == ExpressionType.Parameter)
                    {
                        param = (ParameterExpression)node.Left;
                    }
                    else if (node.Left.NodeType == ExpressionType.Constant)
                    {
                        constant = (ConstantExpression)node.Left;
                    }

                    if (node.Right.NodeType == ExpressionType.Parameter)
                    {
                        param = (ParameterExpression)node.Right;
                    }
                    else if (node.Right.NodeType == ExpressionType.Constant)
                    {
                        constant = (ConstantExpression)node.Right;
                    }

                    if (param != null && constant != null && constant.Type == typeof(int) && (int)constant.Value == 1)
                    {
                        return Expression.Decrement(param);
                    }
                }
                return base.VisitBinary(node);
            }
        }

        public class ConstTransform : ExpressionVisitor
        {
            private Expression ConvertExpression(BinaryExpression node)
            {
                ParameterExpression param = null;
                ParameterExpression replacingParameter;
                ConstantExpression constant = null;

                Expression Left = node.Left;
                Expression Right = node.Right;

                Expression expression = null;

                if (Left.NodeType == ExpressionType.Parameter)
                {
                    replacingParameter = (ParameterExpression)Left;
                    if (_replacing.ContainsKey(replacingParameter.Name))
                    {
                        Expression constantExpr = Expression.Constant(
                            Convert.ChangeType(_replacing[replacingParameter.Name], replacingParameter.Type),
                            replacingParameter.Type
                        );
                        expression = Expression.MakeBinary(node.NodeType, constantExpr, Right);
                        Left = ((BinaryExpression)expression).Left;
                        Right = ((BinaryExpression)expression).Right;
                    }
                }
                else if(Left.NodeType != ExpressionType.Constant)
                {
                    Left = ConvertExpression((BinaryExpression) Left);
                    expression = Expression.MakeBinary(node.NodeType, Left, Right);
                }

                if (Right.NodeType == ExpressionType.Parameter)
                {
                    replacingParameter = (ParameterExpression)Right;
                    if (_replacing.ContainsKey(replacingParameter.Name))
                    {
                        Expression constantExpr = Expression.Constant(
                            Convert.ChangeType(_replacing[replacingParameter.Name], replacingParameter.Type),
                            replacingParameter.Type
                        );
                        expression = Expression.MakeBinary(node.NodeType, Left, constantExpr);
                        Left = ((BinaryExpression)expression).Left;
                        Right = ((BinaryExpression)expression).Right;
                    }
                }
                else if(Right.NodeType != ExpressionType.Constant)
                {
                    Right = ConvertExpression((BinaryExpression)Right);
                    expression = Expression.MakeBinary(node.NodeType, Left, Right);
                }

                return expression ?? node;
            }

            private Dictionary<string, object> _replacing;
            protected override Expression VisitBinary(BinaryExpression node)
            {

                Expression expression = null;
                expression = ConvertExpression(node);

                return expression ??  base.VisitBinary(node);
            }

            public void UpdatExpression(Dictionary<string, object> replacing)
            {
                _replacing = replacing;
            }
        }


        [TestMethod]
        public void TestMethod1()
        {
            var replacing= new Dictionary<string, object>
            {
                { "a", 23},
                {"pr", 75 }
            };

            var constTransform = new ConstTransform();
            constTransform.UpdatExpression(replacing);

            var incrementOrDecrement = new AddToIncrementOrDecrementTransform();

            Expression<Func<int, int, int>> source_exp = (a, m) => a + a + a + (m + 1) * (a + 5) + (m -1);
            var result_exp_const = (constTransform.VisitAndConvert(source_exp, ""));
            Debug.WriteLine(result_exp_const + " " + result_exp_const.Compile().Invoke(3, 4));
            var result_exp_inc_dec = (incrementOrDecrement.VisitAndConvert(source_exp, ""));
            Debug.WriteLine(result_exp_inc_dec + " " + result_exp_inc_dec.Compile().Invoke(3, 4));

            var result_exp = (incrementOrDecrement.VisitAndConvert(result_exp_const, ""));
            Debug.WriteLine(result_exp + " " + result_exp.Compile().Invoke(3,4));
        }
    }
}
